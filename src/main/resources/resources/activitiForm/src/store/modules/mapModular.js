/**
 * @description 地图MapModular
 * @author zr
 */
import { loadModules } from 'esri-loader'
import mapConfig from '@/utils/map/MapConfig'
const state = {
  options: {
    url: mapConfig.initUrl,
    css: mapConfig.cssUrl
  },
  loadModulesArr: [
    'esri/map',
    'esri/SpatialReference',
    'esri/Color',
    'esri/graphic',
    'esri/layers/WebTiledLayer',
    'esri/layers/WMTSLayer',
    'esri/layers/ArcGISDynamicMapServiceLayer',
    'esri/layers/FeatureLayer',
    'esri/layers/GraphicsLayer',
    'esri/layers/ImageParameters',
    'esri/symbols/SimpleFillSymbol',
    'esri/symbols/SimpleLineSymbol',
    'esri/symbols/PictureMarkerSymbol',
    'esri/geometry/Polyline',
    'esri/symbols/TextSymbol',
    'esri/symbols/Font',
    'esri/geometry/Circle',
    'esri/geometry/Polygon',
    'esri/geometry/Point',
    'esri/geometry/webMercatorUtils',
    'esri/dijit/InfoWindow',
    'dextra/dijit/MeasureTools',
    'esri/renderers/SimpleRenderer',
    'esri/renderers/UniqueValueRenderer',
    'esri/tasks/FindTask',
    'esri/tasks/FindParameters',
    'esri/tasks/query',
    'esri/tasks/QueryTask',
    'esri/tasks/Geoprocessor',
    'esri/tasks/FeatureSet',
    'src/EchartsLayer',
    'dojo/domReady!'
  ],
  mapModular: {},
  mapLoader: false
}

const mutations = {
  SET_MAPMODULAR: (state, param) => {
    param.forEach((e) => {
      state.mapModular[e.name] = e.Fn
    })
    state.mapLoader = true
    sessionStorage.setItem(
      'loadModulesArr',
      JSON.stringify(state.loadModulesArr)
    )
    sessionStorage.setItem('options', JSON.stringify(state.options))
  }
}

const actions = {
  initMapModular({ state, commit }) {
    const gisModules = state.loadModulesArr
    const options = state.options
    loadModules(gisModules, options).then((args) => {
      const data = _TDTInstance(args)
      commit('SET_MAPMODULAR', data)
    })
  }
}

const _TDTInstance = function(args) {
  const gisModules = state.loadModulesArr
  const data = args.map((v, k) => {
    let name = gisModules[k].split('/').pop()
    name = name.replace(/^\S/, (s) => s.toUpperCase())
    return {
      name: name,
      Fn: v
    }
  })
  return data
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
